﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class SwipeSlideSelect : MonoBehaviour, IDragHandler, IEndDragHandler
{

    public RectTransform panelHolder;
    private int indexSelected;
    public bool WarpSwipe = true;

    private float percentageDiv = 10f;
    private float valueAfterDiv;
    private RectTransform[] panels;
    private RectTransform panelParent;
    private Vector3 swipeDir;
    private Vector3 swipeGain;
    private Vector3 panelLocation;
    private Vector3 newLocation;

    bool IsSwipeRight => swipeDir.x > 0;
    bool IsSwipeLeft => swipeDir.x < 0;

    Coroutine smoothTransitionCoroutine;

    private void Start()
    {
        panels = new RectTransform[panelHolder.childCount];


        for (int i = 0; i < panels.Length; i++)
        {
            panels[i] = panelHolder.GetChild(i).GetComponent<RectTransform>();
        }

        panelParent = panels[0].parent.GetComponent<RectTransform>();

        panelLocation = panelParent.position;
    }



    public void OnDrag(PointerEventData eventData)
    {
        swipeDir = eventData.pressPosition - eventData.position;

        panelParent.position = panelLocation - new Vector3(swipeDir.x, 0, 0);

        swipeGain = swipeDir;

    }

    public void OnEndDrag(PointerEventData eventData)
    {
        if (WarpSwipe)
        {
            valueAfterDiv = Mathf.Abs(swipeGain.x / percentageDiv);
            if (valueAfterDiv > 50f)
            {
                //proceed warpswipe.
                valueAfterDiv = valueAfterDiv >= 100f ? valueAfterDiv / 30 : valueAfterDiv / 10;
                valueAfterDiv = Mathf.Floor(valueAfterDiv / 2);
                Debug.Log(valueAfterDiv);

                if (IsSwipeRight)
                {
                    indexSelected = Mathf.Clamp(indexSelected + ((int)valueAfterDiv) - 1, 0, panels.Length - 1);
                    newLocation = new Vector3(-panels[indexSelected].anchoredPosition.x, 0f, 0f);
                }
                else if (IsSwipeLeft)
                {
                    indexSelected = Mathf.Clamp(indexSelected - ((int)valueAfterDiv) - 1, 0, panels.Length - 1);
                    newLocation = new Vector3(-panels[indexSelected].anchoredPosition.x, 0f, 0f);
                }



                if (smoothTransitionCoroutine == null)
                    smoothTransitionCoroutine = StartCoroutine(SmoothTransition(panelParent.localPosition, newLocation));
                return;
            }
        }

        if (IsSwipeRight)
        {
            indexSelected = Mathf.Clamp(indexSelected + 1, 0, panels.Length - 1);
            newLocation = new Vector3(-panels[indexSelected].anchoredPosition.x, 0f, 0f);
        }
        else if (IsSwipeLeft)
        {
            indexSelected = Mathf.Clamp(indexSelected - 1, 0, panels.Length - 1);
            newLocation = new Vector3(-panels[indexSelected].anchoredPosition.x, 0f, 0f);
        }


        if (smoothTransitionCoroutine == null)
            smoothTransitionCoroutine = StartCoroutine(SmoothTransition(panelParent.localPosition, newLocation));
    }

    IEnumerator SmoothTransition(Vector3 startPos, Vector3 endPos)
    {
        float i = 0f;
        while (i <= 1f)
        {
            i += Time.deltaTime / 0.3f;
            panelHolder.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0f, 1f, i));
            yield return null;

        }

        panelLocation = panelParent.position;

        smoothTransitionCoroutine = null;
        yield break;
    }
}
